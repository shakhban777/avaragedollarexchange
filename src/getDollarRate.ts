import * as cheerio from "cheerio"
import axios from "axios"

const url = "https://www.kursvaliut.ru/c%D1%80%D0%B5%D0%B4%D0%BD%D0%B8%D0%B9-%D0%BA%D1%83%D1%80%D1%81-%D0%B2%D0%B0%D0%BB%D1%8E%D1%82-%D0%B7%D0%B0-%D0%BC%D0%B5%D1%81%D1%8F%D1%86"

function getRelevantData() {
  let previousMonth = new Date().getMonth()
  const currentMonth = previousMonth + 1

  const currentYear = new Date().getFullYear()
  const previousYear = currentYear - 1

  const currentYearUrl = `${url}-${currentYear}`
  const previousYearUrl = `${url}-${previousYear}`

  const isPreviousMonthFromLastYear = previousMonth === 0

  if (isPreviousMonthFromLastYear) {
    previousMonth = 12;
  }

  const selectorForCurrentMonth = `#currency-table-container > table > tbody > tr:nth-child(${currentMonth}) > td.text-right`
  const selectorForPreviousMonth = `#currency-table-container > table > tbody > tr:nth-child(${previousMonth}) > td.text-right`

  return {
    previousMonth,
    currentMonth,
    isPreviousMonthFromLastYear,
    currentYearUrl,
    previousYearUrl,
    selectorForCurrentMonth,
    selectorForPreviousMonth,
  }
}


type GetDollarRateReturn = {
  nextMonthRate: number
  currentMonthRate: number
}

export const getDollarRate = async (): Promise<GetDollarRateReturn> => {
  const {
    currentMonth,
    previousMonth,
    isPreviousMonthFromLastYear,
    previousYearUrl,
    currentYearUrl,
    selectorForPreviousMonth,
    selectorForCurrentMonth,
  } = getRelevantData();

  console.log(`Fetching dollar rate for current month: ${currentMonth}, previous month: ${previousMonth}`);

  if (isPreviousMonthFromLastYear) {
    const currentMonthRate = await axios.get(previousYearUrl).then(html => {
      const $ = cheerio.load(html.data)
      const rateWithCurrencyCurrentMonth = $(selectorForPreviousMonth).text()
      const currentMonthRate = rateWithCurrencyCurrentMonth.split(" ")[0]

      console.log(`Parsed current month rate: ${currentMonthRate}`);
      return Number(Number(currentMonthRate).toFixed(2))
    }).catch(error => {
      console.error(`Error fetching data from ${previousYearUrl}:`, error);
      throw error; // Re-throw the error to be handled by the caller
    })

    const nextMonthRate = await axios.get(currentYearUrl).then(html => {
      const $ = cheerio.load(html.data)

      const rateWithCurrencyNextMonth = $(selectorForCurrentMonth).text()
      const nextMonthRate = rateWithCurrencyNextMonth.split(" ")[0]
      console.log(`Parsed next month rate: ${nextMonthRate}`);

      return Number(Number(nextMonthRate).toFixed(2))
    }).catch(error => {
      console.error(`Error fetching data from ${currentYearUrl}:`, error);
      throw error; // Re-throw the error to be handled by the caller
    })

    return {
      currentMonthRate,
      nextMonthRate,
    }
  }

  return await axios.get(currentYearUrl).then(html => {
    console.log(`Successfully fetched data from ${currentYearUrl}`);
    const $ = cheerio.load(html.data)

    const rateWithCurrencyNextMonth = $(selectorForCurrentMonth).text()
    console.log(`Raw next month rate: ${rateWithCurrencyNextMonth}`);
    const nextMonthRate = rateWithCurrencyNextMonth.split(" ")[0]

    const rateWithCurrencyCurrentMonth = $(selectorForPreviousMonth).text()
    console.log(`Raw current month rate: ${rateWithCurrencyCurrentMonth}`);
    const currentMonthRate = rateWithCurrencyCurrentMonth.split(" ")[0]

    return {
      nextMonthRate: Number(Number(nextMonthRate).toFixed(2)),
      currentMonthRate: Number(Number(currentMonthRate).toFixed(2)),
    }
  }).catch(error => {
    console.error(`Error fetching data from ${currentYearUrl}:`, error);
    throw error;
  })
}
