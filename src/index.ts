import express from 'express'

import { getDollarRate } from "./getDollarRate"

const PORT = process.env.PORT || 3003
const app = express()

// Middleware
app.use(express.json())

async function start() {
  try {
    app.get('/', (req, res) => {
      res.status(200).json('Server is working!')
    })

    app.get('/getDollarRate', async (req, res) => {
      const dollarRate = await getDollarRate();
      res.status(200).json(dollarRate);
    })

    app.listen(PORT, () => console.log(`Server started on port ${PORT}`))
  } catch (error) {
    console.log(error)
  }
}

void start()
